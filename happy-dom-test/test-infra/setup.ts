import fetch, { Request } from "node-fetch";

beforeAll(() => {
  console.log(fetch, Request);
  global.fetch = fetch;
  global.window.fetch = fetch;
  global.Request = Request;
  global.window.Request = Request;
});
