import HappyDOMEnvironment from '@happy-dom/jest-environment';
import { EnvironmentContext } from '@jest/environment';
import { Config } from '@jest/types';
import { fetch } from "cross-fetch";

export default class CustomEnvironment extends HappyDOMEnvironment {
	constructor(config: Config.ProjectConfig, options?: EnvironmentContext) {
        super(config, options);
	}

	public async setup(): Promise<void> {
        const parent = super.setup();

       this.global.fetch = fetch;
       this.global.window.fetch = fetch;

       return parent;
    }
}
