import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { configureStore } from "@reduxjs/toolkit";
import * as React from "react";
import { Provider } from "react-redux";
import fetch from "node-fetch";

export interface Pokemon {
  name: string;
}

// Define a service using a base URL and expected endpoints
export const pokemonApi = createApi({
  reducerPath: "pokemonApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://test.com/api/v1/",
    fetchFn: fetch,
  }),
  endpoints: (builder) => ({
    getPokemons: builder.query<Pokemon[], void>({
      query: () => `pokemon`,
    }),
  }),
});

const { useGetPokemonsQuery } = pokemonApi;

const store = configureStore({
  reducer: {
    [pokemonApi.reducerPath]: pokemonApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(pokemonApi.middleware),
});

function useRtkQuery() {
  const {
    isLoading,
    isFetching,
    isError,
    data: pokemons,
  } = useGetPokemonsQuery();
  return { isLoading, isFetching, isError, pokemons };
}

function useFetch() {
  const [isLoading, setIsLoading] = React.useState(true);
  const isFetching = isLoading;
  const [isError, setIsError] = React.useState(false);
  const [pokemons, setPokemons] = React.useState<Pokemon[]>();

  React.useEffect(() => {
    fetch("http://test.com/api/v1/pokemon")
      .then(async (value) => {
        const _data = await value.json();
        setPokemons(_data);
      })
      .catch((e) => {
        console.log(e);
        setIsError(true);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return { isLoading, isFetching, isError, pokemons };
}

function Pokemons() {
  const { isLoading, isFetching, isError, pokemons } = useRtkQuery();

  if (isLoading || isFetching) return <div aria-label="Loading">Loading</div>;

  if (isError) return <div>Error</div>;

  return (
    <div>
      {pokemons.map((poke, key) => (
        <div key={key}>Name: {poke.name}</div>
      ))}
    </div>
  );
}

export function IndexPage() {
  return (
    <Provider store={store}>
      <Pokemons />
    </Provider>
  );
}
