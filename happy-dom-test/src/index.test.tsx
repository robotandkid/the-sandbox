import "@testing-library/jest-dom";
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import * as React from "react";
import { IndexPage, Pokemon } from "./index";

const pokemons: Pokemon[] = [{ name: "picachu" }, { name: "demon-spawn" }];

const handlers = [
  rest.get("http://test.com/api/v1/pokemon", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(pokemons));
  }),
  rest.get("///api/v1/pokemon", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(pokemons));
  }),
];

const server = setupServer(...handlers);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("hello world", () => {
  expect(1 + 2).toBe(3);
});

Array.from({ length: 1 }).forEach((index) => {
  test(`gotta catch them all ${index}`, async () => {
    render(<IndexPage />);

    await waitForElementToBeRemoved(() => screen.queryByText(/loading/i));

    expect(screen.getByText(/picachu/i));
    expect(screen.getByText(/demon/i));
  });
});
